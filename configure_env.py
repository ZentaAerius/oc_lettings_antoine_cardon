from django.core.management.utils import get_random_secret_key

with open(".env", "w") as file:
    file.write(f"SECRET_KEY={get_random_secret_key()}\n")
    file.write("SENTRY_DSN=\n")
    file.write("DEBUG=\n")
    file.write("HEROKU_APP_NAME=")
    file.close()
