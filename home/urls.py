from django.urls import path

from home import views


def trigger_error(request):
    return 1 / 0


app_name = 'home'
urlpatterns = [
    path('', views.index, name='index'),
    path('sentry-debug/', trigger_error),
]
